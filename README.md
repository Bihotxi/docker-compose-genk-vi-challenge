# docker-compose-genk-vi-challenge

## Usage

To run the application, open a terminal inside the repository and type the command:

`docker-compose up -d`

Remember to have a `.env` file with the variables
